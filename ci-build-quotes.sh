set -e 


mkdir ${PUBLIC}/quotes/full

Url="https://quotes.rest/qod/categories"
echo "getting $Url"
Res=$(curl -s --header 'Accept: application/json' ${Url}) 

if test $(echo $Res | ${REPO}/bin/jq 'keys[0]' | tr -d '"') = "error" 
then 
  echo "error ratelimit"
  exit 1
else
  if test $(echo $Res | ${REPO}/bin/jq '.success | keys[0] ' | tr -d '"') != "total"
  then 
    echo "error"
    exit 1
  fi
fi

AllCategories=$(echo $Res | ${REPO}/bin/jq '.contents.categories| keys') 


echo "saving categories"

echo $AllCategories > ${PUBLIC}/quotes/allcategories.json
MainCategories=$(cat ${REPO}/quotes/maincategories.json)


i=0
while [ $i -le 15 ]
do
  Cat=$(echo $AllCategories | ${REPO}/bin/jq ".[$i]" | tr -d '"')
  if test $Cat = "null" 
  then 
    echo "break"
    break
  fi
  Url="https://quotes.rest/qod.json?category=${Cat}"
  echo "getting $Url"
  Res=$(curl -s --header 'Accept: application/json' ${Url})

  if test $(echo $Res | ${REPO}/bin/jq 'keys[0]' | tr -d '"') = "error" 
  then 
    echo "error ratelimit"
    exit 1
  else
    if test $(echo $Res | ${REPO}/bin/jq '.success | keys[0] ' | tr -d '"') != "total"
    then 
      echo "error"
      exit 1
    fi
  fi

  echo "saving $Cat"
  echo $Res | ${REPO}/bin/jq '.' > ${PUBLIC}/quotes/full/${Cat}.json
  echo $Res | ${REPO}/bin/jq '.contents.quotes[0] | {quote, author, permalink, category, length}' > ${PUBLIC}/quotes/${Cat}.json

  i=$(( $i + 1 ))
done






