set -e 

apk add git


git clone --depth 1 ${DEPLOY_URL} ${PUBLIC}/.deploy_repo
mv ${PUBLIC}/.deploy_repo/LICENSE ${PUBLIC}/.deploy_repo/CNAME ${PUBLIC}
date +%Y-%m-%dT%H:%M:%S+2:00 > ${PUBLIC}/build-date

mv ${PUBLIC}/* ${PUBLIC}/.deploy_repo
cd ${PUBLIC}/.deploy_repo
git status
git config --global user.name "Gitlab CI"
git config --global user.email "gitlab-ci@5ls.de"
git add .
git commit -m "build #${CI_PIPELINE_ID}" -m "Pipeline: https://gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/pipelines/${CI_PIPELINE_ID}"
git push