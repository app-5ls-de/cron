set -e 



Locations=$(cat ${REPO}/weather/locations.json)
LocationKeys=$(echo $Locations | ${REPO}/bin/jq "keys")

cp ${REPO}/weather/locations.json ${PUBLIC}/weather/locations.json




i=0
while [ $i -le 15 ]
do
  Key=$(echo $LocationKeys | ${REPO}/bin/jq ".[$i]" | tr -d '"')
  if test $Key = "null" 
  then 
    echo "break"
    break
  fi
  Loc=$(echo $Locations | ${REPO}/bin/jq ".${Key}.location" |tr -d '"')
  if test $Key = "null" 
  then 
    echo "no location set"
    exit 1
  fi
  Lang="de"
  Url="https://api.darksky.net/forecast/${DARK_SKY_TOKEN}/${Loc}?exclude=currently,minutely&extend=hourly&lang=${Lang}&units=si"
  echo "getting $Url"
  Res=$(curl -s --header 'Accept: application/json' ${Url})

  if test $(echo $Res | ${REPO}/bin/jq 'keys[1]' | tr -d '"') = "error" 
  then 
    echo "error"
    exit 1
  else
    if test $(echo $Res | ${REPO}/bin/jq 'has("hourly")') = "false"
    then 
      echo "error"
      exit 1
    fi
  fi

  echo "saving $Key"
  echo $Res > ${PUBLIC}/weather/${Key}-de.json

  i=$(( $i + 1 ))
done






